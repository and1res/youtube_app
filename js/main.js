(function(){
    var ref = {};
    var id_videos = [];
    var videos = ["dGghkjpNCQ8","cl3b7dDBLpo","17ozSeGw-fY","PVzljDmoPVs"];
    
    /*function variables(){
        ref.secciones = $("section");
        ref.reproductor = $(".reproductor");
        ref.btn_prehome = $("#btn_prehome");
        ref.btn_home = $("#btn_home");
        ref.btn_reproductor = $("#btn_reproductor");
    }
    function eventos(){
        ref.btn_prehome.click(abrir);
        ref.btn_home.click(abrir);
        ref.btn_reproductor.click(abrir);

        $("#btn1").click(function(){
            var rnd = _.random(0,videos.length-1);
            //loadVideoById({'videoId': 'bHQqvYy5KYo', 'startSeconds': 5, 'endSeconds': 60, 'suggestedQuality': 'large'});
            console.log(rnd+" "+videos[rnd]);
            player.loadVideoById({'videoId': videos[rnd]});
        });
    }
    function inicializarListaVideos(){
        // http://i1.ytimg.com/vi/1lyu1KKwC74/default.jpg
        id_videos.push({
            id:"1lyu1KKwC74"
        });
        id_videos.push({
            id:"8SbUC-UaAxE"
        });
        id_videos.push({
            id:"Qq-I4orlEhE"
        });
        id_videos.push({
            id:"6Ejga4kJUts"
        });
        id_videos.push({
            id:"fxvkI9MTQw4"
        });
    }
    function pintarGaleria(){
        var temp = "";
        var vid;
        for(i in id_videos)
        {
            vid = id_videos[i].id;
            temp += "<img id='img_1' src='http://i1.ytimg.com/vi/"+vid+"/default.jpg' vid='"+vid+"' />";
            temp += "<br>";
        }
        ref.reproductor.html(temp);
    }
    function abrir(e){
        ref.secciones.hide();
        $("."+(e.target.id).split("_")[1]).slideDown();
    }
    function init(){
        variables();
        eventos();
        inicializarListaVideos();
        pintarGaleria();
    }*/
    function init()
    {
        var playListURL = 'http://gdata.youtube.com/feeds/api/playlists/B2A4E1367126848D?v=2&alt=json&callback=?';
        var videoURL= 'http://www.youtube.com/watch?v=';
        $.getJSON(playListURL, function(data) {
            var list_data="";
            $.each(data.feed.entry, function(i, item) {
                var feedTitle = item.title.$t;
                var feedURL = item.link[1].href;
                var fragments = feedURL.split("/");
                var videoID = fragments[fragments.length - 2];
                var url = videoURL + videoID;
                var thumb = "http://img.youtube.com/vi/"+ videoID +"/default.jpg";
                if (videoID !='videos') {
                    list_data += '<li><a href="'+ url +'" title="'+ feedTitle +'"><img alt="'+ feedTitle+'" src="'+ thumb +'"</a></li>';
                }
            });
            $(list_data).appendTo(".cont");
        });
    }
    window.onload = init;
})();