http_path="/"
css_dir= "css"
sass_dir = "sass"
images_dir = "img"
javascripts_dir = "js"

# To enable relative paths to assets via compass helper functions uncomment the following line:
relative_assets = true

line_comments = false